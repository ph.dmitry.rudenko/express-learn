import { PostController } from '../controllers';
import BaseRouter from './base.routes';

class PostRouter extends BaseRouter {
    constructor() {
        super(new PostController());
    }
}

const { router } = new PostRouter();

export default router;
