import { UserController } from '../controllers';
import BaseRouter from './base.routes';

class UsersRouter extends BaseRouter {
    constructor() {
        super(new UserController());
    }
}

const { router } = new UsersRouter();

export default router;
