import { Router } from 'express';
import { PostController } from '../controllers/';

class ViewRouter {
    public router: Router;
    private controller: PostController

    constructor() {
        this.router = Router();
        this.controller = new PostController()

        this.routes()
    }

    routes() {
        this.router.route('/posts').get(this.controller.getPostsPage)
    }
}

const { router } = new ViewRouter()

export default router
