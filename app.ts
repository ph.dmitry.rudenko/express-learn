import express, { Application } from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import { MainController } from './controllers'
import Routes from './routes'
import ViewRouter from './routes/view.routes'
import { DatabaseService } from './services'
import dotenv from 'dotenv'

class App {
    private app: Application
    private mainController: MainController

    constructor() {
        this.app = express()
        this.mainController = new MainController()
    }

    routing() {
        this.app.get('/', this.mainController.getStartPage)
        this.app.use('/', ViewRouter)

        Object.keys(Routes).forEach((key) => {
            this.app.use(`/api/${key}`, Routes[key])
        })
    }

    initPlugins() {
        this.app.use(bodyParser.json())
        this.app.use(morgan('dev'))
    }

    async start() {
        if (process.env.NODE_ENV !== 'production') {
            //await DatabaseService.dropTables()
            await DatabaseService.createTables()
        }

        this.initPlugins()
        this.routing()

        this.app.listen(3000, () => {
            console.log('Server is running on port 3000')
        })
    }
}

dotenv.config()

const app = new App()
app.start()


