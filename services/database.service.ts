import knex from 'knex';
import path from 'path';
import DatabaseSchemas from '../schemas'

class DatabaseService {
  private static instance: DatabaseService;
  private db: any;

  constructor() {
    this.db = knex({
      client: 'sqlite3',
      connection: {
        filename: path.join(__dirname, '../db.sqlite')
      },
      useNullAsDefault: true
    });
  }

  public static getInstance(): DatabaseService {
    if (!DatabaseService.instance) {
      DatabaseService.instance = new DatabaseService();
    }
    return DatabaseService.instance;
  }

  async createTables() {
    for (const schema of DatabaseSchemas) {
      const tableExists = await this.db.schema.hasTable(schema.name);

      if (!tableExists) {
        await this.db.schema.createTable(schema.name, (table: any) => {
          for (const field of schema.fields) {
            table[field.type as string](field.name);
          }
        });
      }
    }
  }

  // only for testing and development
  async dropTables() {
    for (const schema of DatabaseSchemas) {
      const tableExists = await this.db.schema.hasTable(schema.name);

      if (tableExists) {
        await this.db.schema.dropTable(schema.name);
      }
    }
  }

  async create(table: string, data: any) {
    return this.db
      .insert(data)
      .into(table)
      .returning('*')
      .then((rows: any) => rows[0]);
  }

  async readAll(table: string, params: {
    skip: number,
    limit: number
  }) {
    const items = await this.db.select()
      .from(table)
      .limit(params.limit)
      .offset(params.skip)

      const count = await this.db.select().from(table).count().then(
        (rows: any) => rows[0]["count(*)"]
      )

    return {
      items,
      count
    }
  }

  async read(table: string, id: number) {
    return this.db
      .select()
      .from(table)
      .where('id', id)
      .returning('*')
      .then((rows: any) => rows[0]);
  }

  async update(table: string, id: number, newData: any) {
    return this.db(table).where('id', id).update(newData);
  }

  async delete(table: string, id: number) {
    return this.db(table).where('id', id).del();
  }
}

export default DatabaseService.getInstance();
