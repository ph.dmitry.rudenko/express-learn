export default {
    name: 'posts',
    fields: [
        {
            name: 'id',
            type: 'increments'
        },
        {
            name: 'title',
            type: 'string'
        },
        {
            name: 'content',
            type: 'string'
        }
    ]
}