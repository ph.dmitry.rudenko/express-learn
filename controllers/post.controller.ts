import { BaseController } from './base.controller';
import { Request, Response } from 'express';
import fs from 'fs'
import path from 'path'

const PostsViewPath = path.join(__dirname, "../views/posts.html")

export class PostController extends BaseController  {
  constructor () {
    super('posts');
  }

  async getPostsPage(req: Request, res: Response) {
    try {
      const html = await fs.promises.readFile(PostsViewPath, 'utf8');
      
      res.send(html);
    } catch (error) {
      console.error('Error reading file:', error);
      res.status(500).send('Internal Server Error');
    }
  }
}