import { MainController } from './main.controller';
import { UserController } from './user.controller';
import { PostController } from './post.controller';

export {
    MainController,
    UserController,
    PostController,
}