import { BaseController } from './base.controller';

export class UserController extends BaseController  {
  constructor() {
    super('users');
  }
}
