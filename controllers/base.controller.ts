import { Request, Response } from 'express';
import { DatabaseService } from '../services';

export class BaseController {
    private table: string;

    constructor(table: string) {
        this.table = table;

        this.create = this.create.bind(this);
        this.getList = this.getList.bind(this);
        this.getSingle = this.getSingle.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
    }

    async create(req: Request, res: Response) {
        try {
            const data = await DatabaseService.create(this.table, req.body);

            res.status(201);
            res.json({ message: `${this.table} created`, data });
        } catch (error) {
            res.status(500);
            res.json({ message: `Error creating ${this.table}` });
        }
    }

    async getList(req: Request, res: Response) {
        try {
            console.log("getList", req.query)

            const limit = Number(req.query.limit) || 2
            const skip = Number(req.query.skip) || 0

            const data = await DatabaseService.readAll(this.table, {
                limit,
                skip,
            });

            res.status(200);
            res.json({ data });
        } catch (error) {
            res.status(404);
            res.json({ message: `${this.table} not exists` });
        }
    }

    async getSingle(req: Request, res: Response) {
        try {
            const data = await DatabaseService.read(this.table, Number(req.params.id));
            res.status(200);
            res.json({ data, message: `${this.table} read`});
        } catch (error) {
            res.status(404);
            res.json({ message: `${this.table} not founded` });
        }
    }

    async update(req: Request, res: Response) {
        try {
            await DatabaseService.update(this.table, Number(req.params.id), req.body);
            res.status(200);
            res.json({ updatedData: req.body, message: `${this.table} updated` });
        } catch (error) {
            res.status(500);
            throw new Error(`Error updating ${this.table}`);
        }
    }

    async delete(req: Request, res: Response) {
        try {
            await DatabaseService.delete(this.table, Number(req.params.id));
            res.status(200);
            res.json({ message: `${this.table} was  removed` });
        } catch (error) {
            res.status(500);
            throw new Error(`Error removing ${this.table}`);
        }
    }
}